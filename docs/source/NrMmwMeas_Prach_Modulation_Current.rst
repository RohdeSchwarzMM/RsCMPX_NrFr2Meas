Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:MODulation:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:MODulation:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: