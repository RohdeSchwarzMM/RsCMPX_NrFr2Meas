Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NRMMw:MEASurement<Instance>:PRACh
	single: STOP:NRMMw:MEASurement<Instance>:PRACh
	single: ABORt:NRMMw:MEASurement<Instance>:PRACh

.. code-block:: python

	INITiate:NRMMw:MEASurement<Instance>:PRACh
	STOP:NRMMw:MEASurement<Instance>:PRACh
	ABORt:NRMMw:MEASurement<Instance>:PRACh



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_EvmSymbol.rst
	NrMmwMeas_Prach_Modulation.rst
	NrMmwMeas_Prach_Pdynamics.rst
	NrMmwMeas_Prach_State.rst
	NrMmwMeas_Prach_Trace.rst