DftPrecoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DFTPrecoding

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DFTPrecoding



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.BwPart.Pusch.DftPrecoding.DftPrecodingCls
	:members:
	:undoc-members:
	:noindex: