Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: