Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: