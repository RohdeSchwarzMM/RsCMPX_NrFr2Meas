Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Power.TxPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: