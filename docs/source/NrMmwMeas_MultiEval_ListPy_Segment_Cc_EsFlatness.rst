EsFlatness
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Cc.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.segment.cc.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Segment_Cc_EsFlatness_Average.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_EsFlatness_Current.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_EsFlatness_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_EsFlatness_StandardDev.rst