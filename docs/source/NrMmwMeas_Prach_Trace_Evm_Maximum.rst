Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Evm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: