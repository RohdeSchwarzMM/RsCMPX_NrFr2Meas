Merror
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror_Dmrs.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror_Peak.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror_Rms.rst