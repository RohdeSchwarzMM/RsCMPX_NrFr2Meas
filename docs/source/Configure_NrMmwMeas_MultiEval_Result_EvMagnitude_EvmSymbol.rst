EvmSymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:EVMagnitude:EVMSymbol

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:EVMagnitude:EVMSymbol



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Result.EvMagnitude.EvmSymbol.EvmSymbolCls
	:members:
	:undoc-members:
	:noindex: