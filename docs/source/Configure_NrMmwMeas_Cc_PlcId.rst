PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:PLCid

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:PLCid



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: