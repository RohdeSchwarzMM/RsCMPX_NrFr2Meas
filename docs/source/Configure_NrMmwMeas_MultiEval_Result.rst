Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:MODulation
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:SEMask
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:ACLR
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:PDYNamics
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:PMONitor
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:MERRor
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:EVMC
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:IEMissions
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:ESFLatness
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:IQ
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:TXM
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult[:ALL]

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:MODulation
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:SEMask
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:ACLR
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:PDYNamics
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:PMONitor
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:MERRor
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:EVMC
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:IEMissions
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:ESFLatness
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:IQ
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult:TXM
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:RESult[:ALL]



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Result_EvMagnitude.rst