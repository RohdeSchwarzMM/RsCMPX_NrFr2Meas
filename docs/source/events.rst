RsCMPX_NrFr2Meas Events
==========================

.. _Events:

Check the usage in the Getting Started chapter :ref:`here <GetingStarted_Events>`.

.. autoclass:: RsCMPX_NrFr2Meas.CustomFiles.events.Events()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
