Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:ESFLatness:MAXR<nr>:EXTReme
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:ESFLatness:MAXR<nr>:EXTReme

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:ESFLatness:MAXR<nr>:EXTReme
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:ESFLatness:MAXR<nr>:EXTReme



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.EsFlatness.Maxr.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: