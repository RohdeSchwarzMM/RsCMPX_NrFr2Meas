DftPrecoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>[:CC<carrier>]:BWPart:PUSCh:DFTPrecoding

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>[:CC<carrier>]:BWPart:PUSCh:DFTPrecoding



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Cc.BwPart.Pusch.DftPrecoding.DftPrecodingCls
	:members:
	:undoc-members:
	:noindex: