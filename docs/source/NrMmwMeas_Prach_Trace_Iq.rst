Iq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:IQ

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:IQ



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Iq.IqCls
	:members:
	:undoc-members:
	:noindex: