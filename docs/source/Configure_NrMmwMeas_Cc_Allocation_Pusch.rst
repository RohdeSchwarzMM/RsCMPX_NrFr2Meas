Pusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Allocation.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.cc.allocation.pusch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Cc_Allocation_Pusch_Additional.rst
	Configure_NrMmwMeas_Cc_Allocation_Pusch_Enable.rst
	Configure_NrMmwMeas_Cc_Allocation_Pusch_Nlayers.rst
	Configure_NrMmwMeas_Cc_Allocation_Pusch_Sgeneration.rst