Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:MODulation:EXTReme

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:EXTReme
	FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:EXTReme
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:MODulation:EXTReme



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: