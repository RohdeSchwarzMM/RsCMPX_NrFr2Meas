Pcomp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PCOMp

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PCOMp



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Pcomp.PcompCls
	:members:
	:undoc-members:
	:noindex: