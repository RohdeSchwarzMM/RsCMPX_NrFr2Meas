Pmonitor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PMONitor
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PMONitor

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PMONitor
	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PMONitor



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex: