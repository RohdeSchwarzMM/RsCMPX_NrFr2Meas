Blimits<BandLimits>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.nrMmwMeas.multiEval.limit.aclr.nr.caggregation.blimits.repcap_bandLimits_get()
	driver.configure.nrMmwMeas.multiEval.limit.aclr.nr.caggregation.blimits.repcap_bandLimits_set(repcap.BandLimits.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:ACLR:NR:CAGGregation:BLIMits<n>

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:ACLR:NR:CAGGregation:BLIMits<n>



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Aclr.Nr.Caggregation.Blimits.BlimitsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.aclr.nr.caggregation.blimits.clone()