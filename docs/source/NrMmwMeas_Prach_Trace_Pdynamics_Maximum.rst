Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: