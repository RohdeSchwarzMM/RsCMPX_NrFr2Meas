Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MINimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MINimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: