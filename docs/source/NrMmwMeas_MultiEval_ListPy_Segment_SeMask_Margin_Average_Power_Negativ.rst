Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:AVERage:POWer:NEGativ

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:AVERage:POWer:NEGativ



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SeMask.Margin.Average.Power.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: