Positiv
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.Margin.Area.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.seMask.margin.area.positiv.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_SeMask_Margin_Area_Positiv_Average.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_Margin_Area_Positiv_Current.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_Margin_Area_Positiv_Minimum.rst