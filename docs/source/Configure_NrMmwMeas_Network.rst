Network
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:NETWork:RFPSharing

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:NETWork:RFPSharing



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex: