Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Aclr.Nr.Positiv.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: