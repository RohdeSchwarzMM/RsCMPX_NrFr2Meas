StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:SDEViation
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:SDEViation

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:SDEViation
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:SDEViation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pmonitor.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: