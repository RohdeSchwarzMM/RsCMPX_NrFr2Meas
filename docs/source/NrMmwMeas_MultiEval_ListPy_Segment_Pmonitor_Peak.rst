Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:PEAK

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:PEAK



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Pmonitor.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: