SeMask
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.segment.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Average.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Current.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Dallocation.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Margin.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_StandardDev.rst