Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:TOUT
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:REPetition
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCONdition
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:MOEXception
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:PCINdex
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:PFORmat
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:SSYMbol
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:NOPReambles
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:POPReambles
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCSPacing
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:LRSindex
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:ZCZConfig

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:TOUT
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:REPetition
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCONdition
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:MOEXception
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:PCINdex
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:PFORmat
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:SSYMbol
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:NOPReambles
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:POPReambles
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCSPacing
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:LRSindex
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:ZCZConfig



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Prach_Limit.rst
	Configure_NrMmwMeas_Prach_Modulation.rst
	Configure_NrMmwMeas_Prach_PfOffset.rst
	Configure_NrMmwMeas_Prach_Power.rst
	Configure_NrMmwMeas_Prach_Result.rst
	Configure_NrMmwMeas_Prach_Scount.rst
	Configure_NrMmwMeas_Prach_Sindex.rst