Modulation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TDLoffset
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:DPReceiver

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TDLoffset
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:DPReceiver



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Modulation_EePeriods.rst
	Configure_NrMmwMeas_MultiEval_Modulation_EvmSymbol.rst
	Configure_NrMmwMeas_MultiEval_Modulation_EwLength.rst
	Configure_NrMmwMeas_MultiEval_Modulation_Tracking.rst