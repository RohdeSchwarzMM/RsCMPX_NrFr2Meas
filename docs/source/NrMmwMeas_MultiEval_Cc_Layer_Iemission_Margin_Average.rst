Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:IEMission:MARGin:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:IEMission:MARGin:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.Iemission.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: