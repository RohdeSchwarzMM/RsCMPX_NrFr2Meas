Init
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh:DMRS:INIT

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh:DMRS:INIT



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Allocation.Pucch.Dmrs.Init.InitCls
	:members:
	:undoc-members:
	:noindex: