Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PPOWer:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PPOWer:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PPOWer:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PPOWer:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Ppower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: