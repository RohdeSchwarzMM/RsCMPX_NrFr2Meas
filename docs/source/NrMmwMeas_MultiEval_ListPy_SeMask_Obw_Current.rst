Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.Obw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: