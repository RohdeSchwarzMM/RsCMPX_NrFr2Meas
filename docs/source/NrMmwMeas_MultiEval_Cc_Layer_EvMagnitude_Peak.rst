Peak
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.cc.layer.evMagnitude.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude_Peak_Average.rst
	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude_Peak_Current.rst
	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude_Peak_Maximum.rst