NgBandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:NGBandwidth:AGGRegated

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:NGBandwidth:AGGRegated



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Caggregation.NgBandwidth.NgBandwidthCls
	:members:
	:undoc-members:
	:noindex: