Margin
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.seMask.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_SeMask_Margin_All.rst
	NrMmwMeas_MultiEval_SeMask_Margin_Average.rst
	NrMmwMeas_MultiEval_SeMask_Margin_Current.rst
	NrMmwMeas_MultiEval_SeMask_Margin_Minimum.rst