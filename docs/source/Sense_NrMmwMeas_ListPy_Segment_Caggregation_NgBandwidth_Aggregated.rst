Aggregated
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:NGBandwidth:AGGRegated

.. code-block:: python

	SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:NGBandwidth:AGGRegated



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Caggregation.NgBandwidth.Aggregated.AggregatedCls
	:members:
	:undoc-members:
	:noindex: