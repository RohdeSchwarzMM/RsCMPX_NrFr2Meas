Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.test.nrMmwMeas.network.cc.repcap_carrierComponent_get()
	driver.test.nrMmwMeas.network.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Test.NrMmwMeas.Network.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.test.nrMmwMeas.network.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Test_NrMmwMeas_Network_Cc_Frequency.rst