High
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Merror.Rms.High.HighCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.merror.rms.high.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror_Rms_High_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror_Rms_High_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror_Rms_High_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror_Rms_High_StandardDev.rst