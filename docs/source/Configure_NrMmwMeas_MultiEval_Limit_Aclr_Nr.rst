Nr
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Aclr.Nr.NrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.aclr.nr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Limit_Aclr_Nr_Caggregation.rst
	Configure_NrMmwMeas_MultiEval_Limit_Aclr_Nr_Cbandwidth.rst