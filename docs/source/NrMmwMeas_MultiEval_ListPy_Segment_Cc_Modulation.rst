Modulation
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Cc.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.segment.cc.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation_Average.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation_Current.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation_Dallocation.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation_DchType.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation_Dmodulation.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation_StandardDev.rst