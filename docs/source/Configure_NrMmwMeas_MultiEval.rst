MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PFORmat
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:DMODe
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MMODe
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:NSUBframes
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:FSTRucture
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:GHOPping

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PFORmat
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:DMODe
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MMODe
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:NSUBframes
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:FSTRucture
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:GHOPping



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Limit.rst
	Configure_NrMmwMeas_MultiEval_Modulation.rst
	Configure_NrMmwMeas_MultiEval_Mslot.rst
	Configure_NrMmwMeas_MultiEval_Pcomp.rst
	Configure_NrMmwMeas_MultiEval_Pdynamics.rst
	Configure_NrMmwMeas_MultiEval_Power.rst
	Configure_NrMmwMeas_MultiEval_Result.rst
	Configure_NrMmwMeas_MultiEval_Scount.rst
	Configure_NrMmwMeas_MultiEval_Spectrum.rst