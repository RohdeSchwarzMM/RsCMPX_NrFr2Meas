Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:PMONitor:PEAK

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:PMONitor:PEAK



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Pmonitor.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: