Difference<Difference>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrMmwMeas.multiEval.listPy.cc.esFlatness.difference.repcap_difference_get()
	driver.nrMmwMeas.multiEval.listPy.cc.esFlatness.difference.repcap_difference_set(repcap.Difference.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.EsFlatness.Difference.DifferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.esFlatness.difference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Difference_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Difference_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Difference_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Difference_StandardDev.rst