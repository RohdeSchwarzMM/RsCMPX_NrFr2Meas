Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Aclr.Nr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: