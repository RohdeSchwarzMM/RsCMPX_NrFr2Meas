Low
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Perror.Peak.Low.LowCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.perror.peak.low.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Perror_Peak_Low_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Perror_Peak_Low_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Perror_Peak_Low_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Perror_Peak_Low_StandardDev.rst