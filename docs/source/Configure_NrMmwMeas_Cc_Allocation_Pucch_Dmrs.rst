Dmrs
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Allocation.Pucch.Dmrs.DmrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.cc.allocation.pucch.dmrs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Cc_Allocation_Pucch_Dmrs_Did.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_Dmrs_Init.rst