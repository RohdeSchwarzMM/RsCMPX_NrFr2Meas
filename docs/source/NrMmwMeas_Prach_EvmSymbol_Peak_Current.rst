Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.EvmSymbol.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: