NrMmwMeas
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.NrMmwMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.nrMmwMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_NrMmwMeas_ListPy.rst