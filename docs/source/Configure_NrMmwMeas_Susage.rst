Susage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:SUSage

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:SUSage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Susage.SusageCls
	:members:
	:undoc-members:
	:noindex: