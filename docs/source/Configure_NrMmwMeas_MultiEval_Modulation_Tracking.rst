Tracking
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TRACking:TIMing
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TRACking:PHASe
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TRACking:LEVel

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TRACking:TIMing
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TRACking:PHASe
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:TRACking:LEVel



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Modulation.Tracking.TrackingCls
	:members:
	:undoc-members:
	:noindex: