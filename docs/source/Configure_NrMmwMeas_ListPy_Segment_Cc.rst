Cc<CarrierComponentExt>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.configure.nrMmwMeas.listPy.segment.cc.repcap_carrierComponentExt_get()
	driver.configure.nrMmwMeas.listPy.segment.cc.repcap_carrierComponentExt_set(repcap.CarrierComponentExt.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.listPy.segment.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_ListPy_Segment_Cc_Allocation.rst
	Configure_NrMmwMeas_ListPy_Segment_Cc_BwPart.rst
	Configure_NrMmwMeas_ListPy_Segment_Cc_Cbandwidth.rst
	Configure_NrMmwMeas_ListPy_Segment_Cc_Frequency.rst
	Configure_NrMmwMeas_ListPy_Segment_Cc_Nallocations.rst
	Configure_NrMmwMeas_ListPy_Segment_Cc_PlcId.rst
	Configure_NrMmwMeas_ListPy_Segment_Cc_TaPosition.rst