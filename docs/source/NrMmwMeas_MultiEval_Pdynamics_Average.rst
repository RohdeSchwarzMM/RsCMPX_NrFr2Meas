Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: