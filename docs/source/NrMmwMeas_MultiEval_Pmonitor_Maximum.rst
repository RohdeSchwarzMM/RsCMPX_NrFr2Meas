Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pmonitor.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: