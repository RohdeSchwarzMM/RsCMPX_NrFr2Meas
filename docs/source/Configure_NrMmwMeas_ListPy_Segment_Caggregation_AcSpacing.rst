AcSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:ACSPacing

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:ACSPacing



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Caggregation.AcSpacing.AcSpacingCls
	:members:
	:undoc-members:
	:noindex: