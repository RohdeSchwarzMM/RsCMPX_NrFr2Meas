Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QPSK:PERRor

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QPSK:PERRor



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Qpsk.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: