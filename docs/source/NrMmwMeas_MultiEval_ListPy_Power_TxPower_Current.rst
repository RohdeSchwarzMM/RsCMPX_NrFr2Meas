Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Power.TxPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: