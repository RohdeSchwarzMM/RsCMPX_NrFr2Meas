Caggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:MCARrier

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:MCARrier



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.caggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Caggregation_AcSpacing.rst
	Configure_NrMmwMeas_Caggregation_Cbandwidth.rst
	Configure_NrMmwMeas_Caggregation_Frequency.rst
	Configure_NrMmwMeas_Caggregation_NgBandwidth.rst