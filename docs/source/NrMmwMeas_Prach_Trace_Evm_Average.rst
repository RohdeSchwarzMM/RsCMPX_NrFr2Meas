Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Evm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: