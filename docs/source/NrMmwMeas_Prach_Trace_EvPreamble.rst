EvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVPReamble

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVPReamble



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.EvPreamble.EvPreambleCls
	:members:
	:undoc-members:
	:noindex: