MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NRMMw:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:NRMMw:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:NRMMw:MEASurement<Instance>:MEValuation:DELay
	single: TRIGger:NRMMw:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:NRMMw:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:NRMMw:MEASurement<Instance>:MEValuation:SMODe
	single: TRIGger:NRMMw:MEASurement<Instance>:MEValuation:FSYNc

.. code-block:: python

	TRIGger:NRMMw:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:NRMMw:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:NRMMw:MEASurement<Instance>:MEValuation:DELay
	TRIGger:NRMMw:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:NRMMw:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:NRMMw:MEASurement<Instance>:MEValuation:SMODe
	TRIGger:NRMMw:MEASurement<Instance>:MEValuation:FSYNc



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Trigger.NrMmwMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex: