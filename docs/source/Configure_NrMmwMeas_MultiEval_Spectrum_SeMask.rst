SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Spectrum.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: