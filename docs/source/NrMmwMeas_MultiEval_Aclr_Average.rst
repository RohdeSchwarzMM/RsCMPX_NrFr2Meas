Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:ACLR:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:ACLR:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:ACLR:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:ACLR:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: