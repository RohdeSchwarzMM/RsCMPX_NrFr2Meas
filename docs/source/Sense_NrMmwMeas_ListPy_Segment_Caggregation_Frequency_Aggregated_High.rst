High
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:FREQuency:AGGRegated:HIGH

.. code-block:: python

	SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:FREQuency:AGGRegated:HIGH



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Caggregation.Frequency.Aggregated.High.HighCls
	:members:
	:undoc-members:
	:noindex: