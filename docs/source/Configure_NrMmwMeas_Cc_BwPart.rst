BwPart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.BwPart.BwPartCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.cc.bwPart.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Cc_BwPart_Pucch.rst
	Configure_NrMmwMeas_Cc_BwPart_Pusch.rst