Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POWer:NEGativ

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POWer:NEGativ



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Margin.Minimum.Power.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: