Nsymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:NSYMbol

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:NSYMbol



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.Nsymbol.NsymbolCls
	:members:
	:undoc-members:
	:noindex: