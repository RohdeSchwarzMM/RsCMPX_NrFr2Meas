NgBandwidth
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Caggregation.NgBandwidth.NgBandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.nrMmwMeas.listPy.segment.caggregation.ngBandwidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_NrMmwMeas_ListPy_Segment_Caggregation_NgBandwidth_Aggregated.rst