Sgeneration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>[:CC<carrier>]:ALLocation<Allocation>:PUSCh:SGENeration

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>[:CC<carrier>]:ALLocation<Allocation>:PUSCh:SGENeration



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Cc.Allocation.Pusch.Sgeneration.SgenerationCls
	:members:
	:undoc-members:
	:noindex: