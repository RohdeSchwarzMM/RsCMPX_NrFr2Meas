Phbpsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUCCh:PHBPsk

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUCCh:PHBPsk



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.BwPart.Pucch.Phbpsk.PhbpskCls
	:members:
	:undoc-members:
	:noindex: