Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PERRor:PEAK:HIGH:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PERRor:PEAK:HIGH:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PERRor:PEAK:HIGH:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:PERRor:PEAK:HIGH:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Perror.Peak.High.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: