Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Aclr.Nr.Negativ.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: