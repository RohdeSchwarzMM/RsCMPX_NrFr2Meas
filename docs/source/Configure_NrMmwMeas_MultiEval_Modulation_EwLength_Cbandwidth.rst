Cbandwidth<ChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw50 .. Bw400
	rc = driver.configure.nrMmwMeas.multiEval.modulation.ewLength.cbandwidth.repcap_channelBw_get()
	driver.configure.nrMmwMeas.multiEval.modulation.ewLength.cbandwidth.repcap_channelBw_set(repcap.ChannelBw.Bw50)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<bw>

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<bw>



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Modulation.EwLength.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.modulation.ewLength.cbandwidth.clone()