Caggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:SEMask:AREA<area>:CAGGregation

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:SEMask:AREA<area>:CAGGregation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.SeMask.Area.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex: