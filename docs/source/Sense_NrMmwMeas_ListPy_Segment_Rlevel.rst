Rlevel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:RLEVel

.. code-block:: python

	SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:RLEVel



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Rlevel.RlevelCls
	:members:
	:undoc-members:
	:noindex: