Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:EVM:RMS:HIGH:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:EVM:RMS:HIGH:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:EVM:RMS:HIGH:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:EVM:RMS:HIGH:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Evm.Rms.High.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: