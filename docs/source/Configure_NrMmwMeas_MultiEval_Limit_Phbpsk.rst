Phbpsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:FERRor

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:FERRor



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Phbpsk.PhbpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.phbpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Limit_Phbpsk_EsFlatness.rst
	Configure_NrMmwMeas_MultiEval_Limit_Phbpsk_EvMagnitude.rst
	Configure_NrMmwMeas_MultiEval_Limit_Phbpsk_Ibe.rst
	Configure_NrMmwMeas_MultiEval_Limit_Phbpsk_IqOffset.rst
	Configure_NrMmwMeas_MultiEval_Limit_Phbpsk_Merror.rst
	Configure_NrMmwMeas_MultiEval_Limit_Phbpsk_Perror.rst