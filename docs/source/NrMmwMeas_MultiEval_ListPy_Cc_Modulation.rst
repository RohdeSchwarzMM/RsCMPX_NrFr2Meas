Modulation
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Dallocation.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_DchType.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Dmodulation.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Evm.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_FreqError.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_IqOffset.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Merror.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Perror.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Ppower.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Psd.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Terror.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Tpower.rst