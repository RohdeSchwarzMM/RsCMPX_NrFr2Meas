NrMmwMeas
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Test.NrMmwMeas.NrMmwMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.test.nrMmwMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Test_NrMmwMeas_Network.rst