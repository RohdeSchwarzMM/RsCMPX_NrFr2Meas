Pusch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LEADing
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LAGGing

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LEADing
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LAGGing



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Modulation.EePeriods.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex: