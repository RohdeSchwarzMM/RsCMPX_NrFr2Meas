Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Power.TxPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: