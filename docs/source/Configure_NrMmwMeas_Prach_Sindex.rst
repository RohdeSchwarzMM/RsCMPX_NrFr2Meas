Sindex
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:SINDex:AUTO
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:SINDex

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:SINDex:AUTO
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:SINDex



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Sindex.SindexCls
	:members:
	:undoc-members:
	:noindex: