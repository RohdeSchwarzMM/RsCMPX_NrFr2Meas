Modulation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:MODulation:EWLength
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:MODulation:EWPosition

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:MODulation:EWLength
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:MODulation:EWPosition



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: