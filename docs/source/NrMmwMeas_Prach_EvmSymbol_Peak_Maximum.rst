Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.EvmSymbol.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: