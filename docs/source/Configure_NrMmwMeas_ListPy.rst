ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:OSINdex
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:OSINdex
	CONFigure:NRMMw:MEASurement<Instance>:LIST



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_ListPy_Lrange.rst
	Configure_NrMmwMeas_ListPy_Segment.rst