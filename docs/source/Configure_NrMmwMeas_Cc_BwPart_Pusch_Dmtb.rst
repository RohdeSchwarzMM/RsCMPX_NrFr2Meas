Dmtb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTB

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTB



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.BwPart.Pusch.Dmtb.DmtbCls
	:members:
	:undoc-members:
	:noindex: