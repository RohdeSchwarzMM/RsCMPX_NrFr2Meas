Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:SEMask:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:SEMask:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: