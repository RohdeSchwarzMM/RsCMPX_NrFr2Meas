Aggregated
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:CBANdwidth:AGGRegated

.. code-block:: python

	SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:CBANdwidth:AGGRegated



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Caggregation.Cbandwidth.Aggregated.AggregatedCls
	:members:
	:undoc-members:
	:noindex: