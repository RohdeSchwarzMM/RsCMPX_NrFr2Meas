ListPy
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Aclr.rst
	NrMmwMeas_MultiEval_ListPy_Cc.rst
	NrMmwMeas_MultiEval_ListPy_Pmonitor.rst
	NrMmwMeas_MultiEval_ListPy_Power.rst
	NrMmwMeas_MultiEval_ListPy_Segment.rst
	NrMmwMeas_MultiEval_ListPy_SeMask.rst
	NrMmwMeas_MultiEval_ListPy_Sreliability.rst