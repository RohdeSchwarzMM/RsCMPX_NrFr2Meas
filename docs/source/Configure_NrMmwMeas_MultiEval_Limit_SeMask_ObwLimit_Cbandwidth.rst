Cbandwidth<ChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw50 .. Bw400
	rc = driver.configure.nrMmwMeas.multiEval.limit.seMask.obwLimit.cbandwidth.repcap_channelBw_get()
	driver.configure.nrMmwMeas.multiEval.limit.seMask.obwLimit.cbandwidth.repcap_channelBw_set(repcap.ChannelBw.Bw50)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:SEMask:OBWLimit:CBANdwidth<bw>

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:SEMask:OBWLimit:CBANdwidth<bw>



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.SeMask.ObwLimit.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.seMask.obwLimit.cbandwidth.clone()