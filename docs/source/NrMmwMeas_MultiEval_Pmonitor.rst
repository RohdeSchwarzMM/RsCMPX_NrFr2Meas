Pmonitor
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.pmonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Pmonitor_Average.rst
	NrMmwMeas_MultiEval_Pmonitor_Current.rst
	NrMmwMeas_MultiEval_Pmonitor_Maximum.rst
	NrMmwMeas_MultiEval_Pmonitor_Minimum.rst
	NrMmwMeas_MultiEval_Pmonitor_StandardDev.rst