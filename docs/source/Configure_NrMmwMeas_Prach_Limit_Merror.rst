Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:MERRor

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:MERRor



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: