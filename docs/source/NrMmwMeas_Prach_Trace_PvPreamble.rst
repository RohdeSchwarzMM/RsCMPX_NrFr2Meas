PvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PVPReamble

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PVPReamble



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.PvPreamble.PvPreambleCls
	:members:
	:undoc-members:
	:noindex: