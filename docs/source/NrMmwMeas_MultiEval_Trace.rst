Trace
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Trace_Aclr.rst
	NrMmwMeas_MultiEval_Trace_Pdynamics.rst
	NrMmwMeas_MultiEval_Trace_Pmonitor.rst
	NrMmwMeas_MultiEval_Trace_SeMask.rst