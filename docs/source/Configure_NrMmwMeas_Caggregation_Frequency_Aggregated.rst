Aggregated
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:LOW
	single: CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:CENTer
	single: CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:HIGH

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:LOW
	CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:CENTer
	CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:HIGH



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Caggregation.Frequency.Aggregated.AggregatedCls
	:members:
	:undoc-members:
	:noindex: