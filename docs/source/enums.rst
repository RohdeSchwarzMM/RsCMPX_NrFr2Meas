Enums
=========

Band
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Band.B257
	# All values (6x):
	B257 | B258 | B259 | B260 | B261 | B262

BandwidthPart
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandwidthPart.BWP0
	# All values (4x):
	BWP0 | BWP1 | BWP2 | BWP3

ChannelBw
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelBw.B050
	# All values (4x):
	B050 | B100 | B200 | B400

ChannelTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeA.PUCCh
	# All values (2x):
	PUCCh | PUSCh

ConfigType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConfigType.T1
	# All values (2x):
	T1 | T2

CyclicPrefix
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CyclicPrefix.EXTended
	# All values (2x):
	EXTended | NORMal

DmrsInit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DmrsInit.CID
	# All values (2x):
	CID | DID

DuplexModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DuplexModeB.FDD
	# All values (2x):
	FDD | TDD

GhopingInit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GhopingInit.CID
	# All values (2x):
	CID | HID

GroupHopping
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GroupHopping.DISable
	# All values (3x):
	DISable | ENABle | NEITher

Initialization
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Initialization.CID
	# All values (2x):
	CID | DMRSid

Lagging
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Lagging.MS05
	# All values (3x):
	MS05 | MS25 | OFF

Leading
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Leading.MS25
	# All values (2x):
	MS25 | OFF

ListMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListMode.ONCE
	# All values (2x):
	ONCE | SEGMent

LoLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoLevel.CORRect
	# All values (3x):
	CORRect | HIGH | LOW

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MappingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MappingType.A
	# All values (2x):
	A | B

MeasCarrier
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasCarrier.CC1
	# All values (4x):
	CC1 | CC2 | CC3 | CC4

MeasFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasFilter.BANDpass
	# All values (2x):
	BANDpass | GAUSs

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.MELMode
	# All values (2x):
	MELMode | NORMal

MeasureSlot
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasureSlot.ALL
	# All values (2x):
	ALL | UDEF

ModScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModScheme.BPSK
	# All values (6x):
	BPSK | PHBPsk | Q16 | Q256 | Q64 | QPSK

NsValue
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NsValue.NS01
	# Last value:
	value = enums.NsValue.NSU43
	# All values (99x):
	NS01 | NS02 | NS03 | NS04 | NS05 | NS06 | NS07 | NS08
	NS09 | NS10 | NS100 | NS11 | NS12 | NS13 | NS14 | NS15
	NS16 | NS17 | NS18 | NS19 | NS20 | NS21 | NS22 | NS23
	NS24 | NS25 | NS26 | NS27 | NS28 | NS29 | NS30 | NS31
	NS32 | NS35 | NS36 | NS37 | NS38 | NS39 | NS40 | NS41
	NS42 | NS43 | NS44 | NS45 | NS46 | NS47 | NS48 | NS49
	NS50 | NS51 | NS52 | NS53 | NS54 | NS55 | NS56 | NS57
	NS58 | NS59 | NS60 | NS61 | NS62 | NS63 | NS64 | NS65
	NS66 | NS67 | NS68 | NS69 | NS70 | NS71 | NS72 | NS73
	NS74 | NS75 | NS76 | NS77 | NS78 | NS79 | NS80 | NS81
	NS82 | NS83 | NS84 | NS85 | NS86 | NS87 | NS88 | NS89
	NS90 | NS91 | NS92 | NS93 | NS94 | NS95 | NS96 | NS97
	NS98 | NS99 | NSU43

Path
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Path.NETWork
	# All values (2x):
	NETWork | STANdalone

Periodicity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Periodicity.MS05
	# All values (8x):
	MS05 | MS0625 | MS1 | MS10 | MS125 | MS2 | MS25 | MS5

PeriodPreamble
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PeriodPreamble.MS01
	# All values (6x):
	MS01 | MS0125 | MS025 | MS05 | MS10 | MS20

PhaseComp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PhaseComp.CAF
	# All values (3x):
	CAF | OFF | UDEF

PowerClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerClass.PC1
	# All values (4x):
	PC1 | PC2 | PC3 | PC4

PreambleFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PreambleFormat.A1
	# Last value:
	value = enums.PreambleFormat.C2
	# All values (9x):
	A1 | A2 | A3 | B1 | B2 | B3 | B4 | C0
	C2

PucchFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PucchFormat.F0
	# All values (5x):
	F0 | F1 | F2 | F3 | F4

RbwA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwA.K120
	# All values (2x):
	K120 | M1

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetriggerFlag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerFlag.IFPower
	# All values (3x):
	IFPower | OFF | ON

RfConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RfConverter.IRX1
	# Last value:
	value = enums.RfConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.SAL
	# All values (1x):
	SAL

ScSpacing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ScSpacing.S120k
	# All values (2x):
	S120k | S60K

Sharing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Sharing.FSHared
	# All values (3x):
	FSHared | NSHared | OCONnect

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SyncMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncMode.ENHanced
	# All values (4x):
	ENHanced | ESSLot | NORMal | NSSLot

TargetStateA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetStateA.OFF
	# All values (3x):
	OFF | RDY | RUN

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

TimeMask
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeMask.GOO
	# All values (1x):
	GOO

UsedSlots
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UsedSlots.DL
	# All values (3x):
	DL | UL | X

