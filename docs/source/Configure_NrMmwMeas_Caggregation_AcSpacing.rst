AcSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:ACSPacing

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:ACSPacing



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Caggregation.AcSpacing.AcSpacingCls
	:members:
	:undoc-members:
	:noindex: