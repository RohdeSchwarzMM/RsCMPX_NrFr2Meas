Qpsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Qpsk.QpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.qpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Limit_Qpsk_EsFlatness.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qpsk_EvMagnitude.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qpsk_Ibe.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qpsk_IqOffset.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qpsk_Merror.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qpsk_Perror.rst