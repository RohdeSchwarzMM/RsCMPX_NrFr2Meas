DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<carrier>]:MODulation:DCHType

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<carrier>]:MODulation:DCHType



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Cc.Modulation.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: