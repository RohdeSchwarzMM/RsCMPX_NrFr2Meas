Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRMMw:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:NRMMw:MEASurement<Instance>:SCENario



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Route.NrMmwMeas.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.nrMmwMeas.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_NrMmwMeas_Scenario_MaProtocol.rst
	Route_NrMmwMeas_Scenario_Salone.rst