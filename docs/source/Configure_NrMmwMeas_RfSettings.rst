RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:MLOFfset
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LOLevel
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LOFRequency
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LRINterval

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:MLOFfset
	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LOLevel
	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LOFRequency
	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LRINterval



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_RfSettings_Eattenuation.rst
	Configure_NrMmwMeas_RfSettings_LrStart.rst