Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: