Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Margin.Minimum.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: