Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:MAXimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:MAXimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: