Power
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.segment.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Segment_Power_Average.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Power_Current.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Power_Maximum.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Power_Minimum.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Power_StandardDev.rst