Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: