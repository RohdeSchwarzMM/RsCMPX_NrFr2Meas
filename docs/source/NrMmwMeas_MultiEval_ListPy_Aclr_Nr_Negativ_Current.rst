Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Aclr.Nr.Negativ.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: