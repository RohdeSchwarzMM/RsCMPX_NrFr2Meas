ListPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRMMw:MEASurement<Instance>:LIST:MODE

.. code-block:: python

	TRIGger:NRMMw:MEASurement<Instance>:LIST:MODE



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Trigger.NrMmwMeas.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex: