Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.TxPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: