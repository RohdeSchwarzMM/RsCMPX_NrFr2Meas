Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Aclr.Nr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: