Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Trace_Pdynamics_Average.rst
	NrMmwMeas_MultiEval_Trace_Pdynamics_Current.rst
	NrMmwMeas_MultiEval_Trace_Pdynamics_Maximum.rst