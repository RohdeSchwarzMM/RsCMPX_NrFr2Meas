Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Aclr.Nr.Positiv.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: