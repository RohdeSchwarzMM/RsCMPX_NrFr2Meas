Psd
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Psd.PsdCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.psd.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Psd_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Psd_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Psd_Maximum.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Psd_Minimum.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Psd_StandardDev.rst