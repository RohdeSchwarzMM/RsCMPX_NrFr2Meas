EsFlatness
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.cc.layer.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Cc_Layer_EsFlatness_Average.rst
	NrMmwMeas_MultiEval_Cc_Layer_EsFlatness_Current.rst
	NrMmwMeas_MultiEval_Cc_Layer_EsFlatness_Extreme.rst
	NrMmwMeas_MultiEval_Cc_Layer_EsFlatness_StandardDev.rst