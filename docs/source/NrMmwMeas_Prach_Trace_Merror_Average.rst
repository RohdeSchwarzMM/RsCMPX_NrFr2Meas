Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: