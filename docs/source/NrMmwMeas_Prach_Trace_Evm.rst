Evm
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Evm.EvmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.trace.evm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_Trace_Evm_Average.rst
	NrMmwMeas_Prach_Trace_Evm_Current.rst
	NrMmwMeas_Prach_Trace_Evm_Maximum.rst