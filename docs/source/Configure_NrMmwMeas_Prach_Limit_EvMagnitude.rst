EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:EVMagnitude



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: