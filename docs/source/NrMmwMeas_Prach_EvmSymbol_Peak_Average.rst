Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.EvmSymbol.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: