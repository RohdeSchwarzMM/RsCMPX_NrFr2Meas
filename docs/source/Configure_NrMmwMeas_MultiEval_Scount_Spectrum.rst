Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Scount.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex: