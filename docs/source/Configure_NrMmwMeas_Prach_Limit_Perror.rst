Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:PERRor

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:PERRor



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: