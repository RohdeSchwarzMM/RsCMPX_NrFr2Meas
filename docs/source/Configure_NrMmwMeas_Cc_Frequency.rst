Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:FREQuency

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:FREQuency



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: