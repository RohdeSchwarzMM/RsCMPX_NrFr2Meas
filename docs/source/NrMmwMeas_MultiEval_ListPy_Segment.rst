Segment<SEGMent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr512
	rc = driver.nrMmwMeas.multiEval.listPy.segment.repcap_sEGMent_get()
	driver.nrMmwMeas.multiEval.listPy.segment.repcap_sEGMent_set(repcap.SEGMent.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Segment_Aclr.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Pmonitor.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Power.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask.rst