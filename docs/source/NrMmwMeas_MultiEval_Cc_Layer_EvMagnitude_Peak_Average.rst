Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:PEAK:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:PEAK:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:PEAK:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:PEAK:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.EvMagnitude.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: