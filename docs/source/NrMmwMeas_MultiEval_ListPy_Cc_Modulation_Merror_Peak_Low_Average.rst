Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:MERRor:PEAK:LOW:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:MERRor:PEAK:LOW:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:MERRor:PEAK:LOW:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST[:CC<carrier>]:MODulation:MERRor:PEAK:LOW:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Merror.Peak.Low.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: