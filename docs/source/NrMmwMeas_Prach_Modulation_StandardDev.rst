StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:SDEViation
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:SDEViation

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:SDEViation
	FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:SDEViation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: