Pusch
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.BwPart.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.cc.bwPart.pusch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Cc_BwPart_Pusch_DftPrecoding.rst
	Configure_NrMmwMeas_Cc_BwPart_Pusch_Dmta.rst
	Configure_NrMmwMeas_Cc_BwPart_Pusch_Dmtb.rst