Terror
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Terror.TerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.terror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Terror_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Terror_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Terror_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Terror_StandardDev.rst