EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:EVMagnitude

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:EVMagnitude



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Phbpsk.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: