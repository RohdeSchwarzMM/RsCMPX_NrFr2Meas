Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: