Current
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SeMask.Margin.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.segment.seMask.margin.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Margin_Current_Negativ.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Margin_Current_Positiv.rst
	NrMmwMeas_MultiEval_ListPy_Segment_SeMask_Margin_Current_Power.rst