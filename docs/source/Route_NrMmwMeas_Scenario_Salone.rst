Salone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRMMw:MEASurement<Instance>:SCENario:SALone

.. code-block:: python

	ROUTe:NRMMw:MEASurement<Instance>:SCENario:SALone



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Route.NrMmwMeas.Scenario.Salone.SaloneCls
	:members:
	:undoc-members:
	:noindex: