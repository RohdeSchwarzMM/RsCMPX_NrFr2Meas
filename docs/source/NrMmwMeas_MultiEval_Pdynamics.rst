Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Pdynamics_Average.rst
	NrMmwMeas_MultiEval_Pdynamics_Current.rst
	NrMmwMeas_MultiEval_Pdynamics_Maximum.rst
	NrMmwMeas_MultiEval_Pdynamics_Minimum.rst
	NrMmwMeas_MultiEval_Pdynamics_StandardDev.rst