Iemissions
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.Trace.Iemissions.IemissionsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.cc.layer.trace.iemissions.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Cc_Layer_Trace_Iemissions_Average.rst
	NrMmwMeas_MultiEval_Cc_Layer_Trace_Iemissions_Current.rst
	NrMmwMeas_MultiEval_Cc_Layer_Trace_Iemissions_Maximum.rst