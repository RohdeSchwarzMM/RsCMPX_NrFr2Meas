Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: