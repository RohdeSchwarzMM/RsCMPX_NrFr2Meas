Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: