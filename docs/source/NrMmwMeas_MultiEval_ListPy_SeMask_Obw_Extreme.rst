Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.Obw.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: