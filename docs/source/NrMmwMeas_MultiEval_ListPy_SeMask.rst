SeMask
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_SeMask_Dallocation.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_Margin.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_Obw.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_TxPower.rst