Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:EXTReme
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:EXTReme
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:EXTReme

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:EXTReme
	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:EXTReme
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:EXTReme



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.EsFlatness.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: