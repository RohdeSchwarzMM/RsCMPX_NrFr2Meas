Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: