Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.SeMask.Rbw.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: