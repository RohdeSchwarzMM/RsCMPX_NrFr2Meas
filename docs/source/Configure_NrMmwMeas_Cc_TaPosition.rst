TaPosition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:TAPosition

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:TAPosition



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.TaPosition.TaPositionCls
	:members:
	:undoc-members:
	:noindex: