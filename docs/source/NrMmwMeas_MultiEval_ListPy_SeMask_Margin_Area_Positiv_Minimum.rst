Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:POSitiv:MINimum

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:POSitiv:MINimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.Margin.Area.Positiv.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: