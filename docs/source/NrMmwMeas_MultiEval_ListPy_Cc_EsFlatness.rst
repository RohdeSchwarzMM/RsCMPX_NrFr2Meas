EsFlatness
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Difference.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Maxr.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Minr.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Ripple.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_ScIndex.rst