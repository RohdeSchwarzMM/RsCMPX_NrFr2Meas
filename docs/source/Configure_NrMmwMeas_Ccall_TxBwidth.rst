TxBwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:CCALl:TXBWidth:SCSPacing

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:CCALl:TXBWidth:SCSPacing



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Ccall.TxBwidth.TxBwidthCls
	:members:
	:undoc-members:
	:noindex: