Mcarrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:MCARrier

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:MCARrier



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Caggregation.Mcarrier.McarrierCls
	:members:
	:undoc-members:
	:noindex: