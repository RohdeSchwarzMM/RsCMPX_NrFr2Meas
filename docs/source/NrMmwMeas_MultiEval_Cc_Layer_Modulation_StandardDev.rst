StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:SDEViation
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:SDEViation

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:SDEViation
	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:SDEViation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: