Limit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:FERRor

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:LIMit:FERRor



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.prach.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Prach_Limit_EvMagnitude.rst
	Configure_NrMmwMeas_Prach_Limit_Merror.rst
	Configure_NrMmwMeas_Prach_Limit_Pdynamics.rst
	Configure_NrMmwMeas_Prach_Limit_Perror.rst