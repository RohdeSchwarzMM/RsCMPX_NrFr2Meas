Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pmonitor.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: