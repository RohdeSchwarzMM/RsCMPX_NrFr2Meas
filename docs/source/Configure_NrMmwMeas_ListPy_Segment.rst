Segment<SEGMent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr512
	rc = driver.configure.nrMmwMeas.listPy.segment.repcap_sEGMent_get()
	driver.configure.nrMmwMeas.listPy.segment.repcap_sEGMent_set(repcap.SEGMent.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_ListPy_Segment_Aclr.rst
	Configure_NrMmwMeas_ListPy_Segment_Caggregation.rst
	Configure_NrMmwMeas_ListPy_Segment_Cc.rst
	Configure_NrMmwMeas_ListPy_Segment_Ccall.rst
	Configure_NrMmwMeas_ListPy_Segment_Modulation.rst
	Configure_NrMmwMeas_ListPy_Segment_SeMask.rst
	Configure_NrMmwMeas_ListPy_Segment_Setup.rst