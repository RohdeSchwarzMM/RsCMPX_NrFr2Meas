Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:POWer:HDMode

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:POWer:HDMode



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: