Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Power.TxPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: