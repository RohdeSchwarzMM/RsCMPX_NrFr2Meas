UlDl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:ULDL:PERiodicity

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:ULDL:PERiodicity



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.UlDl.UlDlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.ulDl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_UlDl_Pattern.rst