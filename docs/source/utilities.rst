RsCMPX_NrFr2Meas Utilities
==========================

.. _Utilities:

.. autoclass:: RsCMPX_NrFr2Meas.CustomFiles.utilities.Utilities()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
   :member-order: bysource
