Ripple<Ripple>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrMmwMeas.multiEval.listPy.cc.esFlatness.ripple.repcap_ripple_get()
	driver.nrMmwMeas.multiEval.listPy.cc.esFlatness.ripple.repcap_ripple_set(repcap.Ripple.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.EsFlatness.Ripple.RippleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.esFlatness.ripple.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Ripple_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Ripple_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Ripple_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness_Ripple_StandardDev.rst