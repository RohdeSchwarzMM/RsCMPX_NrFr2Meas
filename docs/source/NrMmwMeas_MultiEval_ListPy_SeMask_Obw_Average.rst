Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.Obw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: