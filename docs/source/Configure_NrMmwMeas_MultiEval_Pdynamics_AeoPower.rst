AeoPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LEADing
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LAGGing

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LEADing
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LAGGing



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Pdynamics.AeoPower.AeoPowerCls
	:members:
	:undoc-members:
	:noindex: