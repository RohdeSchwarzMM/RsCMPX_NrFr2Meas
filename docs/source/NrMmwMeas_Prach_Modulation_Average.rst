Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:MODulation:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:MODulation:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:MODulation:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: