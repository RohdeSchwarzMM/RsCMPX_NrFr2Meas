Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: