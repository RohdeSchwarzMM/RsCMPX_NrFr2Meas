Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:LRANge

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:LRANge



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: