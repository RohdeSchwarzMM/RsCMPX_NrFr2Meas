Nallocations
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:NALLocations

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:NALLocations



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Nallocations.NallocationsCls
	:members:
	:undoc-members:
	:noindex: