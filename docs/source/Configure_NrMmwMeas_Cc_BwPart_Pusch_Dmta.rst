Dmta
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTA

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTA



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.BwPart.Pusch.Dmta.DmtaCls
	:members:
	:undoc-members:
	:noindex: