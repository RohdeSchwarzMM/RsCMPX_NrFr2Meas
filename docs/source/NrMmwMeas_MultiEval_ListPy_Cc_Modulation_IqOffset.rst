IqOffset
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_StandardDev.rst