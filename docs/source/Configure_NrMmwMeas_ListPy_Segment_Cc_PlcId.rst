PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>[:CC<carrier>]:PLCid

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>[:CC<carrier>]:PLCid



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Cc.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: