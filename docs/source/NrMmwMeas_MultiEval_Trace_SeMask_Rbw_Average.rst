Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.SeMask.Rbw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: