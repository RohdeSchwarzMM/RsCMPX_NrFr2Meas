Mslot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MSLot

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:MSLot



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Mslot.MslotCls
	:members:
	:undoc-members:
	:noindex: