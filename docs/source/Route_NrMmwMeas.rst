NrMmwMeas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRMMw:MEASurement<Instance>

.. code-block:: python

	ROUTe:NRMMw:MEASurement<Instance>



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Route.NrMmwMeas.NrMmwMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.nrMmwMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_NrMmwMeas_RfSettings.rst
	Route_NrMmwMeas_Scenario.rst