Allocation<AllocationMore>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.configure.nrMmwMeas.cc.allocation.repcap_allocationMore_get()
	driver.configure.nrMmwMeas.cc.allocation.repcap_allocationMore_set(repcap.AllocationMore.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Allocation.AllocationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.cc.allocation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Cc_Allocation_Bwp.rst
	Configure_NrMmwMeas_Cc_Allocation_Ctype.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch.rst
	Configure_NrMmwMeas_Cc_Allocation_Pusch.rst
	Configure_NrMmwMeas_Cc_Allocation_Srs.rst