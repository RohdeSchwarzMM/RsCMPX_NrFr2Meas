Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: