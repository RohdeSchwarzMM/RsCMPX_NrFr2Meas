Sreliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SRELiability

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SRELiability



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Sreliability.SreliabilityCls
	:members:
	:undoc-members:
	:noindex: