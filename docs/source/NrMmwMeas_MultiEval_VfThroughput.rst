VfThroughput
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:VFTHroughput

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:VFTHroughput



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.VfThroughput.VfThroughputCls
	:members:
	:undoc-members:
	:noindex: