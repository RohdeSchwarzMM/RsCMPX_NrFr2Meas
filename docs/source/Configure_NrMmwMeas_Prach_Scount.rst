Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCOunt:MODulation
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCOunt:PDYNamics

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCOunt:MODulation
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:SCOunt:PDYNamics



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: