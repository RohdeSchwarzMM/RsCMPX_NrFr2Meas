Trace
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.cc.layer.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Cc_Layer_Trace_EsFlatness.rst
	NrMmwMeas_MultiEval_Cc_Layer_Trace_Evmc.rst
	NrMmwMeas_MultiEval_Cc_Layer_Trace_EvmSymbol.rst
	NrMmwMeas_MultiEval_Cc_Layer_Trace_Iemissions.rst
	NrMmwMeas_MultiEval_Cc_Layer_Trace_Iq.rst