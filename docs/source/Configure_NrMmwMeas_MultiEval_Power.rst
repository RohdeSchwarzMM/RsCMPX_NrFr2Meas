Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:POWer:HDMode

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:POWer:HDMode



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: