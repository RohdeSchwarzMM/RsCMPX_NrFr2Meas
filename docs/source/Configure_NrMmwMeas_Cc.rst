Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.nrMmwMeas.cc.repcap_carrierComponent_get()
	driver.configure.nrMmwMeas.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Cc_Allocation.rst
	Configure_NrMmwMeas_Cc_BwPart.rst
	Configure_NrMmwMeas_Cc_Cbandwidth.rst
	Configure_NrMmwMeas_Cc_Frequency.rst
	Configure_NrMmwMeas_Cc_Nallocations.rst
	Configure_NrMmwMeas_Cc_NbwParts.rst
	Configure_NrMmwMeas_Cc_PlcId.rst
	Configure_NrMmwMeas_Cc_Sassignment.rst
	Configure_NrMmwMeas_Cc_TaPosition.rst
	Configure_NrMmwMeas_Cc_TxBwidth.rst