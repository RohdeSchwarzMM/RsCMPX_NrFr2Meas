Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SeMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: