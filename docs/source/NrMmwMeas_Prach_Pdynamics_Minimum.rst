Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MINimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MINimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: