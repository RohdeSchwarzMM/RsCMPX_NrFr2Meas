Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_Pdynamics_Average.rst
	NrMmwMeas_Prach_Pdynamics_Current.rst
	NrMmwMeas_Prach_Pdynamics_Maximum.rst
	NrMmwMeas_Prach_Pdynamics_Minimum.rst
	NrMmwMeas_Prach_Pdynamics_StandardDev.rst