Cbandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:CBANdwidth

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:CBANdwidth



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex: