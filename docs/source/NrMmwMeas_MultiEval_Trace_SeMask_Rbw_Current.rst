Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.SeMask.Rbw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: