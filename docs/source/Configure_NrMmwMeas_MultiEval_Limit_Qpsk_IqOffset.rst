IqOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QPSK:IQOFfset

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QPSK:IQOFfset



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Qpsk.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex: