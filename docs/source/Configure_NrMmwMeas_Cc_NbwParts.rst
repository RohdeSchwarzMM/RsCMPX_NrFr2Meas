NbwParts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:NBWParts

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:NBWParts



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.NbwParts.NbwPartsCls
	:members:
	:undoc-members:
	:noindex: