Average
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.seMask.margin.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_SeMask_Margin_Average_Negativ.rst
	NrMmwMeas_MultiEval_SeMask_Margin_Average_Positiv.rst
	NrMmwMeas_MultiEval_SeMask_Margin_Average_Power.rst