Allocation<Allocation>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr1
	rc = driver.configure.nrMmwMeas.listPy.segment.cc.allocation.repcap_allocation_get()
	driver.configure.nrMmwMeas.listPy.segment.cc.allocation.repcap_allocation_set(repcap.Allocation.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Cc.Allocation.AllocationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.listPy.segment.cc.allocation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_ListPy_Segment_Cc_Allocation_Pusch.rst