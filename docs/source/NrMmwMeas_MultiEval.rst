MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NRMMw:MEASurement<Instance>:MEValuation
	single: STOP:NRMMw:MEASurement<Instance>:MEValuation
	single: ABORt:NRMMw:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:NRMMw:MEASurement<Instance>:MEValuation
	STOP:NRMMw:MEASurement<Instance>:MEValuation
	ABORt:NRMMw:MEASurement<Instance>:MEValuation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Aclr.rst
	NrMmwMeas_MultiEval_Cc.rst
	NrMmwMeas_MultiEval_ListPy.rst
	NrMmwMeas_MultiEval_Pdynamics.rst
	NrMmwMeas_MultiEval_Pmonitor.rst
	NrMmwMeas_MultiEval_SeMask.rst
	NrMmwMeas_MultiEval_State.rst
	NrMmwMeas_MultiEval_Trace.rst
	NrMmwMeas_MultiEval_VfThroughput.rst