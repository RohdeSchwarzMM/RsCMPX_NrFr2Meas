Pucch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Allocation.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.cc.allocation.pucch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Cc_Allocation_Pucch_Dmrs.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_Enable.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_Ghopping.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_IcShift.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_IsfHopping.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_Occ.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_ShPrb.rst
	Configure_NrMmwMeas_Cc_Allocation_Pucch_TdoIndex.rst