Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh:ENABle

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh:ENABle



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.Allocation.Pusch.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: