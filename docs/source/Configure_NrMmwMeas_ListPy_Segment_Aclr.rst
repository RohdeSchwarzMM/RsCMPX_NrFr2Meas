Aclr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:ACLR

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:ACLR



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex: