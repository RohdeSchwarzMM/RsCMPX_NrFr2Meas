EvMagnitude
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.cc.layer.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude_Average.rst
	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude_Current.rst
	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude_Maximum.rst
	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude_Peak.rst