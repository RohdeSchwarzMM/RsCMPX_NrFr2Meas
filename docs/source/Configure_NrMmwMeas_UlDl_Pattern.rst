Pattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:ULDL:PATTern

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:ULDL:PATTern



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.UlDl.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex: