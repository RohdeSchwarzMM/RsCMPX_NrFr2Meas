Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: