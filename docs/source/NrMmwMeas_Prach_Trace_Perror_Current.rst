Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: