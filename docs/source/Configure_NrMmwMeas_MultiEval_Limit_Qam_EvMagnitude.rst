EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:EVMagnitude

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:EVMagnitude



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Qam.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: