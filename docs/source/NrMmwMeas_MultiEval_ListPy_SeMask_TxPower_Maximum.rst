Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.TxPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: