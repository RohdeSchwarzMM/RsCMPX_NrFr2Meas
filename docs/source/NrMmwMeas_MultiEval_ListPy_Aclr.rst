Aclr
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Aclr_Dallocation.rst
	NrMmwMeas_MultiEval_ListPy_Aclr_Nr.rst