Eattenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:EATTenuation

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:EATTenuation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.RfSettings.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex: