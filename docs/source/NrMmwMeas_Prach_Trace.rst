Trace
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_Trace_Evm.rst
	NrMmwMeas_Prach_Trace_EvPreamble.rst
	NrMmwMeas_Prach_Trace_Iq.rst
	NrMmwMeas_Prach_Trace_Merror.rst
	NrMmwMeas_Prach_Trace_Pdynamics.rst
	NrMmwMeas_Prach_Trace_Perror.rst
	NrMmwMeas_Prach_Trace_PvPreamble.rst