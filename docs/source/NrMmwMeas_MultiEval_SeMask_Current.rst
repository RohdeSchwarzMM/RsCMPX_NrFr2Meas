Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:SEMask:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:SEMask:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: