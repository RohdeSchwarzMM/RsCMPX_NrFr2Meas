Perror
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.trace.perror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_Trace_Perror_Average.rst
	NrMmwMeas_Prach_Trace_Perror_Current.rst
	NrMmwMeas_Prach_Trace_Perror_Maximum.rst