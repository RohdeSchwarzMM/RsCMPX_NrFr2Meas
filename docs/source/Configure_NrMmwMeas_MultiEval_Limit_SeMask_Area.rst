Area<Area>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.configure.nrMmwMeas.multiEval.limit.seMask.area.repcap_area_get()
	driver.configure.nrMmwMeas.multiEval.limit.seMask.area.repcap_area_set(repcap.Area.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.SeMask.Area.AreaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.seMask.area.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Limit_SeMask_Area_Caggregation.rst
	Configure_NrMmwMeas_MultiEval_Limit_SeMask_Area_Cbandwidth.rst