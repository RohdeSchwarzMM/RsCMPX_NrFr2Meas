SeMask
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Limit_SeMask_Area.rst
	Configure_NrMmwMeas_MultiEval_Limit_SeMask_AtTolerance.rst
	Configure_NrMmwMeas_MultiEval_Limit_SeMask_ObwLimit.rst