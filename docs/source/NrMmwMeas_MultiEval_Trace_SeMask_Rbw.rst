Rbw<Rbw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw120 .. Bw1000
	rc = driver.nrMmwMeas.multiEval.trace.seMask.rbw.repcap_rbw_get()
	driver.nrMmwMeas.multiEval.trace.seMask.rbw.repcap_rbw_set(repcap.Rbw.Bw120)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.SeMask.Rbw.RbwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.trace.seMask.rbw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Trace_SeMask_Rbw_Average.rst
	NrMmwMeas_MultiEval_Trace_SeMask_Rbw_Current.rst
	NrMmwMeas_MultiEval_Trace_SeMask_Rbw_Maximum.rst