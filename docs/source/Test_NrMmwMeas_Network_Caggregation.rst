Caggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:NRMMw:MEASurement<Instance>:NETWork:CAGGregation

.. code-block:: python

	TEST:NRMMw:MEASurement<Instance>:NETWork:CAGGregation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Test.NrMmwMeas.Network.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex: