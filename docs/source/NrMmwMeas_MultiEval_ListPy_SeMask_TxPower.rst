TxPower
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.TxPower.TxPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.seMask.txPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_SeMask_TxPower_Average.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_TxPower_Current.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_TxPower_Maximum.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_TxPower_Minimum.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_TxPower_StandardDev.rst