Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NRMMw:MEASurement<Instance>:PRACh:THReshold
	single: TRIGger:NRMMw:MEASurement<Instance>:PRACh:SLOPe
	single: TRIGger:NRMMw:MEASurement<Instance>:PRACh:TOUT
	single: TRIGger:NRMMw:MEASurement<Instance>:PRACh:MGAP

.. code-block:: python

	TRIGger:NRMMw:MEASurement<Instance>:PRACh:THReshold
	TRIGger:NRMMw:MEASurement<Instance>:PRACh:SLOPe
	TRIGger:NRMMw:MEASurement<Instance>:PRACh:TOUT
	TRIGger:NRMMw:MEASurement<Instance>:PRACh:MGAP



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Trigger.NrMmwMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex: