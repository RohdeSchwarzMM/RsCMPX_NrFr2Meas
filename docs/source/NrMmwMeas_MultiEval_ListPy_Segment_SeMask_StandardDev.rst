StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:SDEViation

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:SDEViation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SeMask.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: