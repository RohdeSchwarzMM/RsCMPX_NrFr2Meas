StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:SDEViation

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:SDEViation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: