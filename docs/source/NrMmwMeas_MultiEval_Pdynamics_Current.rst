Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: