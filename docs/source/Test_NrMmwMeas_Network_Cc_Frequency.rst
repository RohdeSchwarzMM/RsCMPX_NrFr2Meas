Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:NRMMw:MEASurement<Instance>:NETWork[:CC<no>]:FREQuency

.. code-block:: python

	TEST:NRMMw:MEASurement<Instance>:NETWork[:CC<no>]:FREQuency



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Test.NrMmwMeas.Network.Cc.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: