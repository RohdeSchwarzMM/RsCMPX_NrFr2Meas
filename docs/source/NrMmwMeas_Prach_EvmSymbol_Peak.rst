Peak
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.EvmSymbol.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.evmSymbol.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_EvmSymbol_Peak_Average.rst
	NrMmwMeas_Prach_EvmSymbol_Peak_Current.rst
	NrMmwMeas_Prach_EvmSymbol_Peak_Maximum.rst