Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:SEMask:EXTReme

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:SEMask:EXTReme



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: