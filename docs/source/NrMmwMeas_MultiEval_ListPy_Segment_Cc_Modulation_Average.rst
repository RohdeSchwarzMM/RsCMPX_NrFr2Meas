Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<carrier>]:MODulation:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<carrier>]:MODulation:AVERage

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<carrier>]:MODulation:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<carrier>]:MODulation:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Cc.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: