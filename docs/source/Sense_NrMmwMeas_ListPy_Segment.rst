Segment<SEGMent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr512
	rc = driver.sense.nrMmwMeas.listPy.segment.repcap_sEGMent_get()
	driver.sense.nrMmwMeas.listPy.segment.repcap_sEGMent_set(repcap.SEGMent.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.nrMmwMeas.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_NrMmwMeas_ListPy_Segment_Caggregation.rst
	Sense_NrMmwMeas_ListPy_Segment_Rlevel.rst