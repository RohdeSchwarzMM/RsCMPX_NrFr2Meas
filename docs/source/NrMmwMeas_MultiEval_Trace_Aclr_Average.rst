Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: