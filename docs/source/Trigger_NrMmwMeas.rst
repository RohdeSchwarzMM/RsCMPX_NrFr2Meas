NrMmwMeas
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Trigger.NrMmwMeas.NrMmwMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrMmwMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrMmwMeas_ListPy.rst
	Trigger_NrMmwMeas_MultiEval.rst
	Trigger_NrMmwMeas_Prach.rst