Center
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:FREQuency:AGGRegated:CENTer

.. code-block:: python

	SENSe:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:FREQuency:AGGRegated:CENTer



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Caggregation.Frequency.Aggregated.Center.CenterCls
	:members:
	:undoc-members:
	:noindex: