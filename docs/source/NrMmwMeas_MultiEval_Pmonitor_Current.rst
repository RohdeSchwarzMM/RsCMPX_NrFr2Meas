Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pmonitor.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: