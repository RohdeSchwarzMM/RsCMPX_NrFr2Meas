Cbandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Caggregation.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex: