Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:MAXimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:MAXimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.EvMagnitude.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: