Dallocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:DALLocation

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:DALLocation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Dallocation.DallocationCls
	:members:
	:undoc-members:
	:noindex: