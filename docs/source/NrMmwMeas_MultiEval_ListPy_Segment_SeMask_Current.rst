Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SeMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: