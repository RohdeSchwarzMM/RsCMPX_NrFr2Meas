NrMmwMeas
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:SPATh
	single: CONFigure:NRMMw:MEASurement<Instance>:NANTenna
	single: CONFigure:NRMMw:MEASurement<Instance>:BAND
	single: CONFigure:NRMMw:MEASurement<Instance>:NSValue
	single: CONFigure:NRMMw:MEASurement<Instance>:NCARrier
	single: CONFigure:NRMMw:MEASurement<Instance>:IQSWap
	single: CONFigure:NRMMw:MEASurement<Instance>:DOSignaling
	single: CONFigure:NRMMw:MEASurement<Instance>:PCLass

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:SPATh
	CONFigure:NRMMw:MEASurement<Instance>:NANTenna
	CONFigure:NRMMw:MEASurement<Instance>:BAND
	CONFigure:NRMMw:MEASurement<Instance>:NSValue
	CONFigure:NRMMw:MEASurement<Instance>:NCARrier
	CONFigure:NRMMw:MEASurement<Instance>:IQSWap
	CONFigure:NRMMw:MEASurement<Instance>:DOSignaling
	CONFigure:NRMMw:MEASurement<Instance>:PCLass



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.NrMmwMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Caggregation.rst
	Configure_NrMmwMeas_Cc.rst
	Configure_NrMmwMeas_Ccall.rst
	Configure_NrMmwMeas_ListPy.rst
	Configure_NrMmwMeas_MultiEval.rst
	Configure_NrMmwMeas_Network.rst
	Configure_NrMmwMeas_Prach.rst
	Configure_NrMmwMeas_RfSettings.rst
	Configure_NrMmwMeas_Susage.rst
	Configure_NrMmwMeas_UlDl.rst