Cc<CarrierComponentExt>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.nrMmwMeas.multiEval.listPy.cc.repcap_carrierComponentExt_get()
	driver.nrMmwMeas.multiEval.listPy.cc.repcap_carrierComponentExt_set(repcap.CarrierComponentExt.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_EsFlatness.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Iemission.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation.rst