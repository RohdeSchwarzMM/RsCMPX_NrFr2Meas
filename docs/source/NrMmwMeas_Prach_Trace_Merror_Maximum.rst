Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: