Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.TxPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: