Rms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:PMONitor:RMS

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:PMONitor:RMS



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Pmonitor.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: