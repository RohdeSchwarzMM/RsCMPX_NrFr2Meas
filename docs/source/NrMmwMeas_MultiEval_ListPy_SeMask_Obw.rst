Obw
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.seMask.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_SeMask_Obw_Average.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_Obw_Current.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_Obw_Extreme.rst
	NrMmwMeas_MultiEval_ListPy_SeMask_Obw_StandardDev.rst