DpfOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:DPFoffset

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:DPFoffset



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.DpfOffset.DpfOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.modulation.dpfOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_Modulation_DpfOffset_Preamble.rst