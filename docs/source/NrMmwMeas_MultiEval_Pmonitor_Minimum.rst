Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MINimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PMONitor:MINimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pmonitor.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: