Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: