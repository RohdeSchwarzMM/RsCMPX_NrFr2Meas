Layer<Layer>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrMmwMeas.multiEval.cc.layer.repcap_layer_get()
	driver.nrMmwMeas.multiEval.cc.layer.repcap_layer_set(repcap.Layer.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.LayerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.cc.layer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_Cc_Layer_EsFlatness.rst
	NrMmwMeas_MultiEval_Cc_Layer_EvMagnitude.rst
	NrMmwMeas_MultiEval_Cc_Layer_Iemission.rst
	NrMmwMeas_MultiEval_Cc_Layer_Merror.rst
	NrMmwMeas_MultiEval_Cc_Layer_Modulation.rst
	NrMmwMeas_MultiEval_Cc_Layer_Perror.rst
	NrMmwMeas_MultiEval_Cc_Layer_Trace.rst