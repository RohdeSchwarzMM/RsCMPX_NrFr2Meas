Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	FETCh:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:EVMSymbol:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.EvmSymbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: