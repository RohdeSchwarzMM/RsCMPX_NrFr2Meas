Caggregation
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.nrMmwMeas.listPy.segment.caggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_NrMmwMeas_ListPy_Segment_Caggregation_Cbandwidth.rst
	Sense_NrMmwMeas_ListPy_Segment_Caggregation_Frequency.rst
	Sense_NrMmwMeas_ListPy_Segment_Caggregation_NgBandwidth.rst