Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: