Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.SeMask.TxPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: