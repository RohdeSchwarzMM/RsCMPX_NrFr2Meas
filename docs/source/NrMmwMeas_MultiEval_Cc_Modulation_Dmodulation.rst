Dmodulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DMODulation

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DMODulation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Modulation.Dmodulation.DmodulationCls
	:members:
	:undoc-members:
	:noindex: