RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRMMw:MEASurement<Instance>:RFSettings:CONNector

.. code-block:: python

	ROUTe:NRMMw:MEASurement<Instance>:RFSettings:CONNector



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Route.NrMmwMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: