State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:STATe

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:PRACh:STATe



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_State_All.rst