Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: