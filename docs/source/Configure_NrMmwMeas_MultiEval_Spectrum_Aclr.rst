Aclr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Spectrum.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex: