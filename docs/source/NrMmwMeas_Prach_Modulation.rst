Modulation
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_Modulation_Average.rst
	NrMmwMeas_Prach_Modulation_Current.rst
	NrMmwMeas_Prach_Modulation_DpfOffset.rst
	NrMmwMeas_Prach_Modulation_DsIndex.rst
	NrMmwMeas_Prach_Modulation_Extreme.rst
	NrMmwMeas_Prach_Modulation_Nsymbol.rst
	NrMmwMeas_Prach_Modulation_Preamble.rst
	NrMmwMeas_Prach_Modulation_Scorrelation.rst
	NrMmwMeas_Prach_Modulation_StandardDev.rst