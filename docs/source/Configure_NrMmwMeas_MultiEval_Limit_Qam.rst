Qam<Qam>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Order16 .. Order256
	rc = driver.configure.nrMmwMeas.multiEval.limit.qam.repcap_qam_get()
	driver.configure.nrMmwMeas.multiEval.limit.qam.repcap_qam_set(repcap.Qam.Order16)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Limit.Qam.QamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.limit.qam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Limit_Qam_EsFlatness.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qam_EvMagnitude.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qam_FreqError.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qam_Ibe.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qam_IqOffset.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qam_Merror.rst
	Configure_NrMmwMeas_MultiEval_Limit_Qam_Perror.rst