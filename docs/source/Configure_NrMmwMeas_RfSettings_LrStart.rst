LrStart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LRSTart

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:RFSettings:LRSTart



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.RfSettings.LrStart.LrStartCls
	:members:
	:undoc-members:
	:noindex: