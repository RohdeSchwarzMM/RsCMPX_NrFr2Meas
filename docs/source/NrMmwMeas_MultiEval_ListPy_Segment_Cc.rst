Cc<CarrierComponentExt>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.nrMmwMeas.multiEval.listPy.segment.cc.repcap_carrierComponentExt_get()
	driver.nrMmwMeas.multiEval.listPy.segment.cc.repcap_carrierComponentExt_set(repcap.CarrierComponentExt.Nr1)





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.segment.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Segment_Cc_EsFlatness.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Iemission.rst
	NrMmwMeas_MultiEval_ListPy_Segment_Cc_Modulation.rst