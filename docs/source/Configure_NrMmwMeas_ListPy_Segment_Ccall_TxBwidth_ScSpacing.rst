ScSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CCALl:TXBWidth:SCSPacing

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:LIST:SEGMent<no>:CCALl:TXBWidth:SCSPacing



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.ListPy.Segment.Ccall.TxBwidth.ScSpacing.ScSpacingCls
	:members:
	:undoc-members:
	:noindex: