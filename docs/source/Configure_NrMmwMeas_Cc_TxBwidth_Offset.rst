Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:TXBWidth:OFFSet

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>[:CC<no>]:TXBWidth:OFFSet



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Cc.TxBwidth.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: