StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: