Network
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:NRMMw:MEASurement<Instance>:NETWork:NCARrier

.. code-block:: python

	TEST:NRMMw:MEASurement<Instance>:NETWork:NCARrier



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Test.NrMmwMeas.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.test.nrMmwMeas.network.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Test_NrMmwMeas_Network_Caggregation.rst
	Test_NrMmwMeas_Network_Cc.rst