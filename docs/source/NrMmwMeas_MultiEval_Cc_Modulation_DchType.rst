DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DCHType

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DCHType



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Modulation.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: