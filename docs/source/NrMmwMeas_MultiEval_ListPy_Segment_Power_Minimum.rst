Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: