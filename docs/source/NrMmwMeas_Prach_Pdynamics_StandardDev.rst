StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:SDEViation

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:SDEViation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: