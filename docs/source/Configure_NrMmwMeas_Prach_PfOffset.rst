PfOffset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:PFOFfset:AUTO
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:PFOFfset

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:PFOFfset:AUTO
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:PFOFfset



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.PfOffset.PfOffsetCls
	:members:
	:undoc-members:
	:noindex: