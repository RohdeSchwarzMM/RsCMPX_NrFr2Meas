Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:POWer

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:NRMMw:MEASurement<Instance>:MEValuation:SCOunt:POWer



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.MultiEval.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.multiEval.scount.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_MultiEval_Scount_Spectrum.rst