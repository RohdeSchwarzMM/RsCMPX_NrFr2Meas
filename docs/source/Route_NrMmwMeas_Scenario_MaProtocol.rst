MaProtocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRMMw:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:NRMMw:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Route.NrMmwMeas.Scenario.MaProtocol.MaProtocolCls
	:members:
	:undoc-members:
	:noindex: