Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Evm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: