Ppower
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Cc.Modulation.Ppower.PpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.multiEval.listPy.cc.modulation.ppower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Average.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Current.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Maximum.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Minimum.rst
	NrMmwMeas_MultiEval_ListPy_Cc_Modulation_Ppower_StandardDev.rst