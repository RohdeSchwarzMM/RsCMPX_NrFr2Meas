Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:TRACe:EVMSymbol:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:TRACe:EVMSymbol:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:TRACe:EVMSymbol:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:TRACe:EVMSymbol:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Cc.Layer.Trace.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: