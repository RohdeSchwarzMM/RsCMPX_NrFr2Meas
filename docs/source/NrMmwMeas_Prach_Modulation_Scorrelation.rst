Scorrelation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:SCORrelation

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:PRACh:MODulation:SCORrelation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Modulation.Scorrelation.ScorrelationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrMmwMeas.prach.modulation.scorrelation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrMmwMeas_Prach_Modulation_Scorrelation_Preamble.rst