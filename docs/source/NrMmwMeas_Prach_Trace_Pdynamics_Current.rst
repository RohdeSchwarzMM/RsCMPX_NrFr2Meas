Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent
	FETCh:NRMMw:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: