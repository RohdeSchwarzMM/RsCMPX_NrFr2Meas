Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: