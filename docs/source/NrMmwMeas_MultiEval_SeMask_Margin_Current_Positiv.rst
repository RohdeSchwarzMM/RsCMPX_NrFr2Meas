Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:POSitiv

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:POSitiv



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.SeMask.Margin.Current.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: