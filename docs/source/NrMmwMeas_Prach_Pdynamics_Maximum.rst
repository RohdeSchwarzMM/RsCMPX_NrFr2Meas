Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	single: FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	single: CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MAXimum

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	FETCh:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	CALCulate:NRMMw:MEASurement<Instance>:PRACh:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.Prach.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: