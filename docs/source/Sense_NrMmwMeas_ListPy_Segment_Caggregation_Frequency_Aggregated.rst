Aggregated
----------------------------------------





.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Sense.NrMmwMeas.ListPy.Segment.Caggregation.Frequency.Aggregated.AggregatedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.nrMmwMeas.listPy.segment.caggregation.frequency.aggregated.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_NrMmwMeas_ListPy_Segment_Caggregation_Frequency_Aggregated_Center.rst
	Sense_NrMmwMeas_ListPy_Segment_Caggregation_Frequency_Aggregated_High.rst
	Sense_NrMmwMeas_ListPy_Segment_Caggregation_Frequency_Aggregated_Low.rst