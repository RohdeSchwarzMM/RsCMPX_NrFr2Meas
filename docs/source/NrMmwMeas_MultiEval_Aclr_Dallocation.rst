Dallocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:ACLR:DALLocation

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:ACLR:DALLocation



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Aclr.Dallocation.DallocationCls
	:members:
	:undoc-members:
	:noindex: