Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRMMw:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: CALCulate:NRMMw:MEASurement<Instance>:MEValuation:ACLR:CURRent

.. code-block:: python

	READ:NRMMw:MEASurement<Instance>:MEValuation:ACLR:CURRent
	FETCh:NRMMw:MEASurement<Instance>:MEValuation:ACLR:CURRent
	CALCulate:NRMMw:MEASurement<Instance>:MEValuation:ACLR:CURRent



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: