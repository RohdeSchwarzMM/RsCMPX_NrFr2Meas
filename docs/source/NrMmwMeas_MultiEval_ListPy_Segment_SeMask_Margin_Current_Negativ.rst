Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:CURRent:NEGativ

.. code-block:: python

	FETCh:NRMMw:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:CURRent:NEGativ



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.NrMmwMeas.MultiEval.ListPy.Segment.SeMask.Margin.Current.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: