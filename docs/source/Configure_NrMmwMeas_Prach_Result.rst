Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:RESult:MODulation
	single: CONFigure:NRMMw:MEASurement<Instance>:PRACh:RESult:PDYNamics

.. code-block:: python

	CONFigure:NRMMw:MEASurement<Instance>:PRACh:RESult:MODulation
	CONFigure:NRMMw:MEASurement<Instance>:PRACh:RESult:PDYNamics



.. autoclass:: RsCMPX_NrFr2Meas.Implementations.Configure.NrMmwMeas.Prach.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmwMeas.prach.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmwMeas_Prach_Result_All.rst